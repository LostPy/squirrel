mod app;
mod components;
mod pages;

use app::SquirrelApp;
use iced::{window, Sandbox, Settings};

fn main() -> iced::Result {
    SquirrelApp::run(Settings {
        id: Some("org.squirrel.Squirrel".to_owned()),
        antialiasing: true,
        window: window::Settings {
            position: window::Position::Centered,
            min_size: Some((850, 700)),
            ..Default::default()
        },
        ..Default::default()
    })
}
