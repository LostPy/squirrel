use iced::{
    widget::{Column, Row, Rule, Space},
    Element, Length,
};
use squirrel_core::User;

use crate::components::login::{LoginMessage, LoginWidget, SignUpMessage, SignUpWidget};

#[derive(Debug, Clone)]
pub enum LoginPageMessage {
    Login(LoginMessage),
    Signup(SignUpMessage),
}

pub struct LoginPage {
    show_sign_up: bool,
    users: Vec<User>,
    login_widget: LoginWidget,
    signup_widget: SignUpWidget,
}

impl LoginPage {
    pub fn new() -> Self {
        Self {
            show_sign_up: false,
            users: Vec::new(),
            login_widget: LoginWidget::new(Vec::new()),
            signup_widget: SignUpWidget::new(),
        }
    }

    pub fn update(&mut self, message: LoginPageMessage) {
        match message {
            LoginPageMessage::Login(LoginMessage::CreateUser) => {
                self.show_sign_up = !self.show_sign_up
            }
            LoginPageMessage::Login(LoginMessage::LoginClicked { username, password }) => {
                let user_index = self
                    .users
                    .iter()
                    .position(|user| user.name == username)
                    .unwrap();
                if self.users[user_index].login(username, password) {
                    self.login_widget.clear_inputs();
                }
            }
            LoginPageMessage::Login(msg) => self.login_widget.update(msg),
            LoginPageMessage::Signup(SignUpMessage::Cancel) => {
                self.show_sign_up = false;
                self.signup_widget.update(SignUpMessage::Cancel);
            }
            LoginPageMessage::Signup(SignUpMessage::Create(name, password, confirm)) => {
                if password == confirm {
                    self.users.push(User::new(name, password));
                    self.login_widget
                        .set_usernames(self.users.iter().map(|user| user.name.clone()).collect());
                    self.show_sign_up = false;
                    self.signup_widget.update(SignUpMessage::Cancel);
                }
            }
            LoginPageMessage::Signup(msg) => self.signup_widget.update(msg),
        }
    }

    pub fn view(&self) -> Element<LoginPageMessage> {
        let mut row = Row::new()
            .spacing(40)
            .width(Length::Fill)
            .height(Length::FillPortion(3))
            .push(Space::with_width(Length::FillPortion(3)))
            .push(self.login_widget.view().map(LoginPageMessage::Login));
        if self.show_sign_up {
            row = row
                .push(Rule::vertical(20))
                .push(self.signup_widget.view().map(LoginPageMessage::Signup));
        }
        row = row.push(Space::with_width(Length::FillPortion(3)));
        Column::new()
            .spacing(20)
            .width(Length::Fill)
            .height(Length::Fill)
            .push(Space::with_height(Length::FillPortion(2)))
            .push(row)
            .push(Space::with_height(Length::FillPortion(2)))
            .into()
    }
}
