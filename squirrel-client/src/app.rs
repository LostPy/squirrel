use crate::pages::{LoginPage, LoginPageMessage};
use iced::Sandbox;
use squirrel_core::User;

#[derive(Debug)]
pub enum MessageApp {
    Login(LoginPageMessage),
}

#[derive(Debug)]
pub enum SquirrelState {
    Login,
    UpdatingTranctions(User),
    Main(User),
}

pub struct SquirrelApp {
    state: SquirrelState,
    login_page: LoginPage,
}

impl Sandbox for SquirrelApp {
    type Message = MessageApp;

    fn new() -> Self {
        Self {
            state: SquirrelState::Login,
            login_page: LoginPage::new(),
        }
    }

    fn title(&self) -> String {
        String::from("Squirrel")
    }

    fn update(&mut self, message: Self::Message) {
        match message {
            MessageApp::Login(login_msg) => self.login_page.update(login_msg),
        }
    }

    fn view(&self) -> iced::Element<'_, Self::Message> {
        match self.state {
            SquirrelState::Login => self.login_page.view().map(MessageApp::Login),
            _ => unreachable!(),
        }
    }
}
