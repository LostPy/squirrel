pub mod filter;
pub mod list_transactions;
pub mod login;
pub mod party;
pub mod transaction;

#[macro_use]
mod helpers {
    #[macro_export]
    macro_rules! normal_button {
        ($text:expr) => {
            Button::new(Container::new(
                Text::new($text)
                    .width(Length::Fill)
                    .size(14)
                    .horizontal_alignment(Horizontal::Center)
                    .vertical_alignment(Vertical::Center),
            ))
            .width(Length::Units(80))
            .height(Length::Units(30))
        };
    }

    #[macro_export]
    macro_rules! textinput {
        ($text:expr, $msg:expr) => {
            TextInput::new("", $text, $msg).width(Length::Fill)
        };
    }

    #[macro_export]
    macro_rules! label {
        ($text:expr) => {
            Text::new($text)
                .width(Length::Fill)
                .size(14)
                .horizontal_alignment(Horizontal::Left)
                .vertical_alignment(Vertical::Center)
        };
    }

    #[macro_export]
    macro_rules! title_label {
        ($text:expr) => {
            Text::new($text)
                .width(Length::Fill)
                .size(16)
                .horizontal_alignment(Horizontal::Left)
                .vertical_alignment(Vertical::Center)
        };
    }
}
