use iced::{
    alignment::{Horizontal, Vertical},
    widget::{Button, Column, Container, PickList, Row, Rule, Text, TextInput},
    Element, Length,
};

use crate::{label, normal_button, textinput, title_label};

#[derive(Debug, Clone)]
pub enum LoginMessage {
    UsernameChanged(String),
    PasswordChanged(String),
    LoginClicked { username: String, password: String },
    CreateUser,
}

pub struct LoginWidget {
    usernames: Vec<String>,
    current_username: String,
    current_password: String,
    login_button_enable: bool,
}

impl LoginWidget {
    pub fn new(usernames: Vec<String>) -> Self {
        Self {
            usernames,
            current_username: String::new(),
            current_password: String::new(),
            login_button_enable: false,
        }
    }

    pub fn set_usernames(&mut self, usernames: Vec<String>) {
        self.usernames = usernames;
        if self.usernames.len() > 0 {
            self.current_username = self.usernames[0].clone();
        }
    }

    pub fn clear_inputs(&mut self) {
        if self.usernames.len() > 0 {
            self.current_username = self.usernames[0].clone();
        } else {
            self.current_username = String::new();
        }
        self.current_password = String::new();
    }

    fn on_login_input_changed(&mut self) {
        self.login_button_enable =
            !self.current_username.is_empty() && !self.current_password.is_empty()
    }

    fn on_username_changed(&mut self, new: String) {
        self.current_username = new;
        self.current_password = String::new();
        self.on_login_input_changed();
    }

    fn on_password_changed(&mut self, new: String) {
        self.current_password = new;
        self.on_login_input_changed();
    }

    pub fn update(&mut self, message: LoginMessage) {
        match message {
            LoginMessage::UsernameChanged(name) => self.on_username_changed(name),
            LoginMessage::PasswordChanged(pw) => self.on_password_changed(pw),
            _ => {}
        }
    }

    pub fn view(&self) -> Element<LoginMessage> {
        Container::new(
            Column::new()
                .spacing(10)
                .width(Length::Fill)
                .height(Length::Fill)
                .push(title_label!("Login"))
                .push(Rule::horizontal(15))
                .push(label!("Username"))
                .push(PickList::new(
                    self.usernames.as_slice(),
                    if self.usernames.len() > 0 {
                        Some(self.current_username.clone())
                    } else {
                        None
                    },
                    LoginMessage::UsernameChanged,
                ))
                .push(label!("Password"))
                .push(textinput!(&self.current_password, LoginMessage::PasswordChanged).password())
                .push(
                    Row::new()
                        .spacing(10)
                        .push(if self.login_button_enable {
                            normal_button!("Login").on_press(LoginMessage::LoginClicked {
                                username: self.current_username.clone(),
                                password: self.current_password.clone(),
                            })
                        } else {
                            normal_button!("Login")
                        })
                        .push(normal_button!("Create user").on_press(LoginMessage::CreateUser)),
                ),
        )
        .width(Length::FillPortion(2))
        .height(Length::Fill)
        .into()
    }
}

#[derive(Debug, Clone)]
pub enum SignUpMessage {
    UsernameChanged(String),
    PasswordChanged(String),
    ConfirmPasswordChanged(String),
    Cancel,
    Create(String, String, String),
}

pub struct SignUpWidget {
    current_username: String,
    current_password: String,
    current_confirm: String,
    create_button_enable: bool,
}

impl SignUpWidget {
    pub fn new() -> Self {
        Self {
            current_username: String::new(),
            current_password: String::new(),
            current_confirm: String::new(),
            create_button_enable: false,
        }
    }

    fn on_signup_input_changed(&mut self) {
        self.create_button_enable = !self.current_username.is_empty()
            && !self.current_password.is_empty()
            && !self.current_confirm.is_empty()
    }

    fn on_username_changed(&mut self, new: String) {
        self.current_username = new;
        self.on_signup_input_changed();
    }

    fn on_password_changed(&mut self, new: String) {
        self.current_password = new;
        self.on_signup_input_changed();
    }

    fn on_confirm_changed(&mut self, new: String) {
        self.current_confirm = new;
        self.on_signup_input_changed();
    }

    fn on_new_cancel(&mut self) {
        self.current_username = String::new();
        self.current_password = String::new();
        self.current_confirm = String::new();
    }

    pub fn update(&mut self, message: SignUpMessage) {
        match message {
            SignUpMessage::UsernameChanged(name) => self.on_username_changed(name),
            SignUpMessage::PasswordChanged(pw) => self.on_password_changed(pw),
            SignUpMessage::ConfirmPasswordChanged(pw) => self.on_confirm_changed(pw),
            SignUpMessage::Cancel => self.on_new_cancel(),
            _ => {}
        }
    }

    pub fn view(&self) -> Element<SignUpMessage> {
        Container::new(
            Column::new()
                .width(Length::Fill)
                .height(Length::Fill)
                .spacing(10)
                .push(title_label!("Create a new user"))
                .push(Rule::horizontal(15))
                .push(label!("Username"))
                .push(textinput!(
                    &self.current_username,
                    SignUpMessage::UsernameChanged
                ))
                .push(label!("Password"))
                .push(textinput!(&self.current_password, SignUpMessage::PasswordChanged).password())
                .push(label!("Confirm Password"))
                .push(
                    textinput!(&self.current_confirm, SignUpMessage::ConfirmPasswordChanged)
                        .password(),
                )
                .push(
                    Row::new()
                        .spacing(10)
                        .push(if self.create_button_enable {
                            normal_button!("Create").on_press(SignUpMessage::Create(
                                self.current_username.clone(),
                                self.current_password.clone(),
                                self.current_confirm.clone(),
                            ))
                        } else {
                            normal_button!("Create")
                        })
                        .push(normal_button!("Cancel").on_press(SignUpMessage::Cancel)),
                ),
        )
        .width(Length::FillPortion(2))
        .height(Length::Fill)
        .into()
    }
}
