use iced::{
    alignment::{Horizontal, Vertical},
    widget::{Button, Column, Container, Row, Space, Text, TextInput},
    Element, Length,
};
use squirrel_core::{transaction::Category, PeriodicTransaction, Transaction};

use crate::{label, normal_button, textinput};

#[derive(Debug, Clone)]
pub enum TransactionMessage {
    Save,
    Delete,
    ObjectChanged(String),
    CategoryChanged(String),
    PartyChanged(String),
}

pub struct TransactionWidget {
    transaction: Option<Transaction>,
    save_button_enable: bool,
}

impl TransactionWidget {
    pub fn new(transaction: Transaction) -> Self {
        Self {
            transaction: Some(transaction),
            save_button_enable: false,
        }
    }

    pub fn empty() -> Self {
        Self {
            transaction: None,
            save_button_enable: false,
        }
    }

    pub fn update(&mut self, message: TransactionMessage) {
        match message {
            _ => {}
        }
    }

    pub fn view(&self) -> Element<TransactionMessage> {
        let object: &str;
        let category: Category;
        let party_name: &str;
        if let Some(transaction) = &self.transaction {
            object = transaction.object.as_str();
            category = transaction.category.clone();
            party_name = transaction.party.name.as_str();
        } else {
            object = "";
            category = Category::None;
            party_name = "";
        }

        Container::new(
            Column::new()
                .spacing(15)
                .width(Length::Fill)
                .height(Length::Fill)
                .push(object_field(object))
                .push(
                    Row::new()
                        .spacing(25)
                        .width(Length::Fill)
                        .height(Length::Fill)
                        .push(
                            Column::new()
                                .spacing(15)
                                .width(Length::Fill)
                                .height(Length::Fill)
                                .push(category_field(String::from(category))),
                        )
                        .push(
                            Column::new()
                                .spacing(15)
                                .width(Length::Fill)
                                .height(Length::Fill)
                                .push(party_field(party_name)),
                        ),
                )
                .push(buttons()),
        )
        .width(Length::Fill)
        .height(Length::Units(200))
        .into()
    }
}

pub struct PeriodicTransactionWidget {
    periodic_transaction: Option<PeriodicTransaction>,
    save_button_enable: bool,
}

impl PeriodicTransactionWidget {
    pub fn new(periodic_transaction: PeriodicTransaction) -> Self {
        Self {
            periodic_transaction: Some(periodic_transaction),
            save_button_enable: false,
        }
    }

    pub fn empty() -> Self {
        Self {
            periodic_transaction: None,
            save_button_enable: false,
        }
    }

    pub fn update(&mut self, message: TransactionMessage) {
        match message {
            _ => {}
        }
    }

    pub fn view(&self) -> Element<TransactionMessage> {
        let object: &str;
        let category: Category;
        let party_name: &str;
        if let Some(transaction) = &self.periodic_transaction {
            object = transaction.object.as_str();
            category = transaction.category.clone();
            party_name = transaction.party.name.as_str();
        } else {
            object = "";
            category = Category::None;
            party_name = "";
        }
        Container::new(
            Column::new()
                .spacing(15)
                .width(Length::Fill)
                .height(Length::Fill)
                .push(object_field(object))
                .push(
                    Row::new()
                        .spacing(25)
                        .width(Length::Fill)
                        .height(Length::Fill)
                        .push(
                            Column::new()
                                .spacing(15)
                                .width(Length::Fill)
                                .height(Length::Fill)
                                .push(category_field(String::from(category))),
                        )
                        .push(
                            Column::new()
                                .spacing(15)
                                .width(Length::Fill)
                                .height(Length::Fill)
                                .push(party_field(party_name)),
                        ),
                )
                .push(buttons()),
        )
        .width(Length::Fill)
        .height(Length::Units(200))
        .into()
    }
}

fn object_field(object: &str) -> Row<TransactionMessage> {
    Row::new()
        .spacing(10)
        .width(Length::Fill)
        .height(Length::Fill)
        .push(label!("Object"))
        .push(textinput!(object, TransactionMessage::ObjectChanged))
}

fn category_field<'a>(category: String) -> Row<'a, TransactionMessage> {
    Row::new()
        .spacing(10)
        .width(Length::Fill)
        .height(Length::Fill)
        .push(label!("Category"))
        .push(textinput!(&category, TransactionMessage::CategoryChanged))
}

fn party_field(party_name: &str) -> Row<TransactionMessage> {
    Row::new()
        .spacing(10)
        .width(Length::Fill)
        .height(Length::Fill)
        .push(label!("Other Party"))
        .push(textinput!(party_name, TransactionMessage::PartyChanged))
}

fn buttons<'a>() -> Row<'a, TransactionMessage> {
    Row::new()
        .spacing(10)
        .width(Length::Fill)
        .height(Length::Fill)
        .push(Space::with_width(Length::FillPortion(2)))
        .push(normal_button!("Save").on_press(TransactionMessage::Save))
        .push(normal_button!("Delete").on_press(TransactionMessage::Delete))
}
