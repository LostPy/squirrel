use iced::{
    alignment::{Horizontal, Vertical},
    widget::{scrollable::Properties, Column, Container, Scrollable, Text},
    Element, Length,
};
use squirrel_core::{PeriodicTransaction, Transaction};

use crate::title_label;

use super::transaction::{PeriodicTransactionWidget, TransactionMessage, TransactionWidget};

#[derive(Debug, Clone)]
pub enum ListTransactionsMessage {
    TransactionMessage(usize, TransactionMessage),
    TransactionAdd,
    TransactionDelete(usize),
}

pub struct ListTransactionWidget {
    transactions: Vec<TransactionWidget>,
}

impl ListTransactionWidget {
    pub fn new(transactions: Vec<Transaction>) -> Self {
        let mut transaction_widgets: Vec<TransactionWidget> = Vec::from_iter(
            transactions
                .into_iter()
                .map(|transaction| TransactionWidget::new(transaction)),
        );
        transaction_widgets.push(TransactionWidget::empty()); // Use to create new transaction
        Self {
            transactions: transaction_widgets,
        }
    }

    pub fn update(&mut self, message: ListTransactionsMessage) {
        match message {
            _ => {}
        }
    }

    pub fn view(&self) -> Element<ListTransactionsMessage> {
        let mut scrollable_content = Column::new()
            .spacing(5)
            .width(Length::Fill)
            .height(Length::Fill);
        for (index, transaction) in self.transactions.iter().enumerate() {
            scrollable_content = scrollable_content.push(
                transaction
                    .view()
                    .map(move |msg| ListTransactionsMessage::TransactionMessage(index, msg)),
            );
        }
        Container::new(
            Column::new()
                .spacing(20)
                .width(Length::Fill)
                .height(Length::Fill)
                .push(title_label!("Transactions"))
                .push(
                    Scrollable::new(scrollable_content)
                        .height(Length::Fill)
                        .vertical_scroll(Properties::new().width(20).margin(15).scroller_width(25)),
                ),
        )
        .width(Length::Fill)
        .height(Length::Fill)
        .into()
    }
}

pub struct ListPeriodicTransactionWidget {
    transactions: Vec<PeriodicTransactionWidget>,
}

impl ListPeriodicTransactionWidget {
    pub fn new(transactions: Vec<PeriodicTransaction>) -> Self {
        let mut transaction_widgets: Vec<PeriodicTransactionWidget> = Vec::from_iter(
            transactions
                .into_iter()
                .map(|transaction| PeriodicTransactionWidget::new(transaction)),
        );
        transaction_widgets.push(PeriodicTransactionWidget::empty()); // Use to create new transaction
        Self {
            transactions: transaction_widgets,
        }
    }

    pub fn update(&mut self, message: ListTransactionsMessage) {
        match message {
            _ => {}
        }
    }

    pub fn view(&self) -> Element<ListTransactionsMessage> {
        let mut scrollable_content = Column::new()
            .spacing(5)
            .width(Length::Fill)
            .height(Length::Fill);
        for (index, transaction) in self.transactions.iter().enumerate() {
            scrollable_content = scrollable_content.push(
                transaction
                    .view()
                    .map(move |msg| ListTransactionsMessage::TransactionMessage(index, msg)),
            );
        }
        Container::new(
            Column::new()
                .spacing(20)
                .width(Length::Fill)
                .height(Length::Fill)
                .push(title_label!("Transactions"))
                .push(
                    Scrollable::new(scrollable_content)
                        .height(Length::Fill)
                        .vertical_scroll(Properties::new().width(20).margin(15).scroller_width(25)),
                ),
        )
        .width(Length::Fill)
        .height(Length::Fill)
        .into()
    }
}
