use serde::{Deserialize, Serialize};

#[derive(Debug, Deserialize, Serialize)]
pub enum Currency {
    Euro,
    Dollar,
    PoundSterling,
    SwissFranc,
    Yen,
    Renminbi,
    Rupee,
    Other { name: String, symbol: String },
}

impl Currency {
    pub fn name(&self) -> String {
        match self {
            Self::Euro => "Euro".to_owned(),
            Self::Dollar => "Dollar".to_owned(),
            Self::PoundSterling => "Pound Sterling".to_owned(),
            Self::SwissFranc => "Swiss Franc".to_owned(),
            Self::Yen => "Yen".to_owned(),
            Self::Renminbi => "Renminbi".to_owned(),
            Self::Rupee => "Ruppe".to_owned(),
            Self::Other { name, symbol } => name.clone(),
        }
    }

    pub fn symbol(&self) -> String {
        match self {
            Self::Euro => "€".to_owned(),
            Self::Dollar => "$".to_owned(),
            Self::PoundSterling => "£".to_owned(),
            Self::SwissFranc => "CHF".to_owned(),
            Self::Yen => "¥".to_owned(),
            Self::Renminbi => "¥".to_owned(),
            Self::Rupee => "₹".to_owned(),
            Self::Other { name, symbol } => symbol.clone(),
        }
    }
}
