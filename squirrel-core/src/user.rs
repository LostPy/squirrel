use crate::currency::Currency;
use crate::{PeriodicTransaction, Transaction};
use ring::rand::{SecureRandom, SystemRandom};
use ring::{digest, pbkdf2};
use serde::{Deserialize, Serialize};
use std::num::NonZeroU32;

const CREDENTIAL_LEN: usize = digest::SHA256_OUTPUT_LEN;
const HASH_N_ITER: u32 = 3;

#[derive(Debug, Deserialize, Serialize)]
pub struct User {
    pub name: String,
    password: [u8; CREDENTIAL_LEN],
    usalt: [u8; CREDENTIAL_LEN],
    pub transactions: Vec<Transaction>,
    pub future_transactions: Vec<Transaction>,
    pub periodic_transactions: Vec<PeriodicTransaction>,
    pub total: f64,
    pub currency: Currency,
}

impl User {
    pub fn new(name: String, password: String) -> Self {
        let rng = SystemRandom::new();
        let n_iter = NonZeroU32::new(HASH_N_ITER).unwrap();
        let mut salt = [0u8; CREDENTIAL_LEN];
        rng.fill(&mut salt).unwrap();

        let mut pbkdf2_hash = [0u8; CREDENTIAL_LEN];
        pbkdf2::derive(
            pbkdf2::PBKDF2_HMAC_SHA256,
            n_iter,
            &salt,
            password.as_bytes(),
            &mut pbkdf2_hash,
        );

        Self {
            name,
            password: pbkdf2_hash,
            usalt: salt,
            transactions: Vec::new(),
            future_transactions: Vec::new(),
            periodic_transactions: Vec::new(),
            total: 0f64,
            currency: Currency::Euro,
        }
    }

    pub fn login(&self, name: String, password: String) -> bool {
        let n_iter = NonZeroU32::new(HASH_N_ITER).unwrap();
        self.name == name
            && pbkdf2::verify(
                pbkdf2::PBKDF2_HMAC_SHA256,
                n_iter,
                &self.usalt,
                password.as_bytes(),
                &self.password,
            )
            .is_ok()
    }
}
