use chrono::{Duration, Local, NaiveDate};
use serde::{Deserialize, Serialize};

use crate::transaction::{Amount, Category, Party, Transaction};

#[derive(Debug, Deserialize, Serialize)]
pub struct PeriodicTransaction {
    pub object: String,
    pub period: Period,
    pub start: NaiveDate,
    previous_date: Option<NaiveDate>,
    pub amount: Amount,
    pub category: Category,
    pub party: Party,
}

impl PeriodicTransaction {
    pub fn new(
        object: String,
        period: Period,
        start: NaiveDate,
        amount: Amount,
        category: Category,
        party: Party,
    ) -> Self {
        Self {
            object,
            period,
            start,
            previous_date: None,
            amount,
            category,
            party,
        }
    }

    pub fn emit(&mut self) -> Vec<Transaction> {
        let mut news = Vec::new();
        let today = Local::now().date_naive();
        if self.previous_date.is_none() && today >= self.start {
            news.push(Transaction::new(
                self.object.clone(),
                self.start,
                self.party.clone(),
                self.amount,
                self.category.clone(),
            ));
            self.previous_date.replace(self.start);
        }

        let period_duration = self.period.duration();
        let mut future = self.previous_date.unwrap() + period_duration;
        while today > future {
            news.push(Transaction::new(
                self.object.clone(),
                future,
                self.party.clone(),
                self.amount,
                self.category.clone(),
            ));
            future += period_duration;
        }
        self.previous_date.replace(future - period_duration);
        news
    }
}

#[derive(Debug, Clone, Copy, Deserialize, Serialize)]
pub enum Period {
    Months(u8),
    Weeks(u8),
    Days(u8),
}

impl Period {
    pub fn days(&self) -> i64 {
        match self {
            Self::Months(months) => *months as i64 * 30,
            Self::Weeks(weeks) => *weeks as i64 * 7,
            Self::Days(days) => *days as i64,
        }
    }

    pub fn duration(&self) -> Duration {
        Duration::days(self.days())
    }
}
