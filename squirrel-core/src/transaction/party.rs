use serde::{Deserialize, Serialize};

#[derive(Debug, Clone, Deserialize, Serialize)]
pub struct Party {
    pub name: String,
    pub party_type: PartyType,
}

impl Party {
    pub fn new(name: String, party_type: PartyType) -> Self {
        Self { name, party_type }
    }
}

#[derive(Debug, Clone, Copy, Deserialize, Serialize)]
pub enum PartyType {
    Person,
    Company,
    State,
    Organization,
}
