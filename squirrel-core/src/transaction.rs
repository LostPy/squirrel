use chrono::{Local, NaiveDate};
use serde::{Deserialize, Serialize};

pub use party::{Party, PartyType};
pub use periodic::{Period, PeriodicTransaction};

mod party;
mod periodic;

#[derive(Debug, Deserialize, Serialize)]
pub struct Transaction {
    pub object: String,
    pub date: NaiveDate,
    pub party: Party,
    pub amount: Amount,
    pub category: Category,
}

impl Transaction {
    pub fn new(
        object: String,
        date: NaiveDate,
        party: Party,
        amount: Amount,
        category: Category,
    ) -> Self {
        Self {
            object,
            date,
            party,
            amount,
            category,
        }
    }
}

#[derive(Debug, Clone, Deserialize, Serialize)]
pub enum Category {
    None,
    Food,
    Entertainment,
    House,
    Other(String),
}

impl From<Category> for String {
    fn from(value: Category) -> Self {
        match value {
            Category::None => "Undefined".to_string(),
            Category::Food => "Food".to_string(),
            Category::Entertainment => "Entertainment".to_string(),
            Category::House => "Rent".to_string(),
            Category::Other(category) => category,
        }
    }
}

#[derive(Debug, Clone, Copy, Deserialize, Serialize)]
pub enum Amount {
    Income(f64),
    Expense(f64),
}

impl From<f64> for Amount {
    fn from(value: f64) -> Self {
        if value < 0. {
            Self::Expense(value)
        } else {
            Self::Income(value)
        }
    }
}

impl From<Amount> for f64 {
    fn from(value: Amount) -> Self {
        match value {
            Amount::Income(value) => value,
            Amount::Expense(value) => -value,
        }
    }
}
