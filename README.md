# Squirrel

Squirrel is an user interface made with [Iced][iced] to manage your money transactions.

## Features

 * Add / Remove single transaction manually
 * Add / Remove periodic transactions

## Structure

 This application is splitted in 3 crates:

 * `squirrel-client`: The GUI client made with [Iced][iced]
 * `squirrel-core`: The core package with all structures and other component to define your transactions
 * `squirrel-io`: Package to save/load your data in local crypted files


## License
This software is under the GPL V3 license:

```
Copyright (C) 2023  LostPy <https://www.gitlab.com/LostPy>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
```

[iced]: https://docs.rs/iced/0.4.2/iced/index.html
