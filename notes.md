# Notes

## Idées

 * Faire une version utilisant une bdd et une version utilisant des fichiers (cryptés) - voir RPM
 * Pour la version utilisant des fichiers :
   - Diviser les principales parties d'un compte en plusieurs fichiers et les chargers quand c'est nécessaire (moins rapide mais moins de mémoire utilisé)
   - Base de données : requêtes quand nécessaire
